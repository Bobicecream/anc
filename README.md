# Adversarial Neural Cryptography

We propose the new training method which is a little different from the paper
["Learning to Protect Communications with Adversarial Neural
Cryptography"](https://arxiv.org/abs/1610.06918).

> We ask whether neural networks can learn to use secret keys to protect
> information from other neural networks. Specifically, we focus on ensuring
> confidentiality properties in a multiagent system, and we specify those
> properties in terms of an adversary. Thus, a system may consist of neural
> networks named Alice and Bob, and we aim to limit what a third neural
> network named Eve learns from eavesdropping on the communication between
> Alice and Bob. We do not prescribe specific cryptographic algorithms to
> these neural networks; instead, we train end-to-end, adversarially.
> We demonstrate that the neural networks can learn how to perform forms of
> encryption and decryption, and also how to apply these operations
> selectively in order to meet confidentiality goals.

This code allows you to train an encryptor/decryptor/adversary triplet
and evaluate the encryptor/decryptor on randomly generated input and key
pairs.

## Prerequisites

Requires Tensorflow 1.0 or later.

Requires six, seaborn, matplotlib.

## Training and evaluating

After installing TensorFlow and ensuring that your paths are configured
appropriately:

### Training

	$ python v0_eval.py --save-model "directory"
    
or

	$ python vv1_eval.py --save-model "directory"

e.g.

	$ mkdir tmp
	$ python vv1_eval.py --save-model tmp

This will begin training a fresh model.  If and when the model becomes
sufficiently well-trained, it will reset the Eve model multiple times
and retrain it from scratch, outputting the accuracy thus obtained
in each run.

### Evaluating

```
$ python attack.py --load-model "directory/base_name"
```

e.g.

```
$ python attack.py --load-model tmp/cry-ite0
```

This will show reconstruction error of Eve when it attacks an encryptor.
Mostly, the larger the reconstruction error, the better the encryptor.
You can compare diferent encryptors constructed by v0\_eval.py and vv1\_eval.py.

## Differences from the paper

The training method about training data are different from the paper.

## Contact information

This model repository is maintained by Bob Teng
([Bobicecream](https://gitlab.com/Bobicecream))

