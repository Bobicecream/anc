# TensorFlow Python 3 compatibility
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import tensorflow as tf
from myconst import *



def batch_of_random_bools(batch_size, n):
  """Return a batch of random "boolean" numbers.

  Args:
    batch_size:  Batch size dimension of returned tensor.
    n:  number of entries per batch.

  Returns:
    A [batch_size, n] tensor of "boolean" numbers, where each number is
    preresented as -1 or 1.
  """

  as_int = tf.random_uniform(
      [batch_size, n], minval=0, maxval=2, dtype=tf.int32)
  expanded_range = (as_int * 2) - 1
  return tf.cast(expanded_range, tf.float32)


class AdversarialCrypto(object):
  """Primary model implementation class for Adversarial Neural Crypto.

  This class contains the code for the model itself,
  and when created, plumbs the pathways from Alice to Bob and
  Eve, creates the optimizers and loss functions, etc.

  Attributes:
    eve_loss:  Eve's loss function.
    bob_loss:  Bob's loss function.  Different units from eve_loss.
    eve_optimizer:  A tf op that runs Eve's optimizer.
    bob_optimizer:  A tf op that runs Bob's optimizer.
    bob_reconstruction_loss:  Bob's message reconstruction loss,
      which is comparable to eve_loss.
    reset_eve_vars:  Execute this op to completely reset Eve.
  """

  def get_message_and_key(self, input_method):
    """Generate random pseudo-boolean key and message values."""

    batch_size = tf.placeholder_with_default(FLAGS.batch_size, shape=[])

    if input_method == 'random key':
      in_m = batch_of_random_bools(batch_size, TEXT_SIZE)
      in_k = batch_of_random_bools(batch_size, TEXT_SIZE)
    elif input_method == 'feed key':
      in_m = batch_of_random_bools(batch_size, TEXT_SIZE)
      self.in_k = tf.placeholder(shape=[FLAGS.batch_size, TEXT_SIZE], dtype=tf.float32)
      in_k = self.in_k #temporarily in_k
    elif input_method == 'feed msg and key':
      self.in_m = tf.placeholder(shape=[FLAGS.batch_size, TEXT_SIZE], dtype=tf.float32)
      in_m = self.in_m #temporarily in_m
      self.in_k = tf.placeholder(shape=[FLAGS.batch_size, TEXT_SIZE], dtype=tf.float32)
      in_k = self.in_k #temporarily in_k
    else:
      print('unknown input method!!!')
      exit(1)

    return in_m, in_k

  def model(self, collection, message, key=None, kind='default'):
    if kind == 'default':
      r = self.model_origin_update(collection, message, key)
    elif kind == 'origin2016':
      r = self.model_origin2016(collection, message, key)
    elif kind == 'attack':
      r = self.model_attack(collection, message, key)
    elif kind == 'attack2':
      r = self.model_attack2(collection, message, key)
    elif kind == 'softmax':
      r = self.model_softmax(collection, message, key)
    elif kind == 'piecewise':
      r = self.model_piecewise(collection, message, key)
    else:
      print('unknwon model kind')
      exit(1)
    return r
  def model_origin2016(self, collection, message, key=None):
    if key is not None:
      combined_message = tf.concat(axis=1, values=[message, key])
    else:
      combined_message = message

    # Ensure that all variables created are in the specified collection.
    with tf.contrib.framework.arg_scope(
        [tf.contrib.layers.fully_connected, tf.contrib.layers.conv2d],
        variables_collections=[collection]):

      fc = tf.contrib.layers.fully_connected(
          combined_message,
          TEXT_SIZE + KEY_SIZE,
          biases_initializer=tf.constant_initializer(0.0),
          activation_fn=tf.nn.sigmoid)

      # Perform a sequence of 1D convolutions (by expanding the message out to 2D
      # and then squeezing it back down).
      fc = tf.expand_dims(fc, 2)
      # window_size, input_channel -> output_size_percnn, output_channel
      # 4,1 -> 1,2
      conv = tf.contrib.layers.conv2d(
          fc, 2, 4, 1, 'SAME', activation_fn=tf.nn.sigmoid)
      # 2,2 -> 1, 4
      conv = tf.contrib.layers.conv2d(
          conv, 4, 2, 2, 'SAME', activation_fn=tf.nn.sigmoid)
      # 1,4 -> 1, 4
      conv = tf.contrib.layers.conv2d(
          conv, 4, 1, 1, 'SAME', activation_fn=tf.nn.sigmoid)
      # 1,4 -> 1, 1
      conv = tf.contrib.layers.conv2d(
          conv, 1, 1, 1, 'SAME', activation_fn=tf.nn.tanh)
      conv = tf.squeeze(conv, 2)
      return conv
  def model_origin_update(self, collection, message, key=None):
    if key is not None:
      combined_message = tf.concat(axis=1, values=[message, key])
    else:
      combined_message = message

    # Ensure that all variables created are in the specified collection.
    with tf.contrib.framework.arg_scope(
        [tf.contrib.layers.fully_connected, tf.contrib.layers.conv2d],
        variables_collections=[collection]):

      fc = tf.contrib.layers.fully_connected(
          combined_message,
          TEXT_SIZE + KEY_SIZE,
          biases_initializer=tf.constant_initializer(0.0),
          activation_fn=None)

      # Perform a sequence of 1D convolutions (by expanding the message out to 2D
      # and then squeezing it back down).
      fc = tf.expand_dims(fc, 2)
      # 2,1 -> 1,2
      conv = tf.contrib.layers.conv2d(
          fc, 2, 2, 2, 'SAME', activation_fn=tf.nn.sigmoid)
      # 1,2 -> 1, 2
      conv = tf.contrib.layers.conv2d(
          conv, 2, 1, 1, 'SAME', activation_fn=tf.nn.sigmoid)
      # 1,2 -> 1, 1
      conv = tf.contrib.layers.conv2d(
          conv, 1, 1, 1, 'SAME', activation_fn=tf.nn.tanh)
      conv = tf.squeeze(conv, 2)
      return conv
  def model_softmax(self, collection, message, key=None):
    if key is not None:
      combined_message = tf.concat(axis=1, values=[message, key])
    else:
      combined_message = message

    #temp = tf.get_variable(name="temperature")
    # Ensure that all variables created are in the specified collection.
    with tf.contrib.framework.arg_scope(
        [tf.contrib.layers.fully_connected, tf.contrib.layers.conv2d],
        variables_collections=[collection]):

      fc = tf.contrib.layers.fully_connected(
          combined_message,
          TEXT_SIZE + KEY_SIZE,
          biases_initializer=tf.constant_initializer(0.0),
          activation_fn=None)

      # Perform a sequence of 1D convolutions (by expanding the message out to 2D
      # and then squeezing it back down).
      fc = tf.expand_dims(fc, 2)
      # 2,1 -> 1,2
      conv = tf.contrib.layers.conv2d(
          fc, 2, 2, 2, 'SAME', activation_fn=tf.nn.sigmoid)
      # 1,2 -> 1, 2
      conv = tf.contrib.layers.conv2d(
          conv, 2, 1, 1, 'SAME', activation_fn=tf.nn.sigmoid)
      # 1,2 -> 1, 1
      conv = tf.contrib.layers.conv2d(
          conv, 1, 1, 1, 'SAME', activation_fn=tf.nn.sigmoid)
      conv = tf.squeeze(conv, 2)
      #temp = tf.Variable(1.0, name="temperature", trainable=True)
      #soft = tf.log(conv) / self.temp
      #oth = tf.log(1.0 - conv) / self.temp
      #soft = tf.exp(soft) / ( tf.exp(soft) + tf.exp(oth) )
      soft = tf.pow(conv, 1/self.temp)
      oth = tf.pow(1.0 - conv, 1/self.temp)
      soft = soft / ( soft + oth )
      soft = 2*soft - 1.0;
      return soft
  def model_piecewise(self, collection, message, key=None):
    if key is not None:
      combined_message = tf.concat(axis=1, values=[message, key])
    else:
      combined_message = message

    # Ensure that all variables created are in the specified collection.
    with tf.contrib.framework.arg_scope(
        [tf.contrib.layers.fully_connected, tf.contrib.layers.conv2d],
        variables_collections=[collection]):

      fc = tf.contrib.layers.fully_connected(
          combined_message,
          TEXT_SIZE + KEY_SIZE,
          biases_initializer=tf.constant_initializer(0.0),
          activation_fn=None)

      # Perform a sequence of 1D convolutions (by expanding the message out to 2D
      # and then squeezing it back down).
      fc = tf.expand_dims(fc, 2)
      # 2,1 -> 1,2
      conv = tf.contrib.layers.conv2d(
          fc, 2, 2, 2, 'SAME', activation_fn=tf.nn.sigmoid)
      # 1,2 -> 1, 2
      conv = tf.contrib.layers.conv2d(
          conv, 2, 1, 1, 'SAME', activation_fn=tf.nn.sigmoid)
      # 1,2 -> 1, 1
      conv = tf.contrib.layers.conv2d(
          conv, 1, 1, 1, 'SAME', activation_fn=tf.nn.tanh)
      conv = tf.squeeze(conv, 2)
      conv = 2*conv
      conv = tf.clip_by_value(conv, -1.0, 1.0)
      f1 = tf.shape(conv)
      #f2 = tf.constant(0.5, shape=(4096,16))
      #f2 = tf.constant(0.5)
      #f3 = tf.conttant(1.0, shape=f1)
      #f4 = tf.conttant(-1.0, shape=f1)
      #conv = tf.case({tf.less(tf.abs(conv), f2): lambda: tf.tanh(1.0), tf.greater_equal(conv, f2): lambda: constant(1.0)},
      #    default=lambda: tf.constant(-1.0), exclusive=True)
      #conv = tf.nn.relu(conv)
      return conv
  def model_attack(self, collection, message, key=None):
    if key is not None:
      combined_message = tf.concat(axis=1, values=[message, key])
    else:
      combined_message = message

    # Ensure that all variables created are in the specified collection.
    with tf.contrib.framework.arg_scope(
        [tf.contrib.layers.fully_connected, tf.contrib.layers.conv2d],
        variables_collections=[collection]):

      fc = tf.contrib.layers.fully_connected(
          combined_message,
          TEXT_SIZE + KEY_SIZE,
          biases_initializer=tf.constant_initializer(0.0),
          activation_fn=None)

      fc = tf.contrib.layers.fully_connected(
          fc,
          8*(TEXT_SIZE + KEY_SIZE),
          biases_initializer=tf.constant_initializer(0.0),
          activation_fn=tf.nn.sigmoid)
      conv = tf.contrib.layers.fully_connected(
          fc,
          KEY_SIZE,
          biases_initializer=tf.constant_initializer(0.0),
          activation_fn=tf.nn.tanh)

      self.prekey = conv

      combined_message = tf.concat([message, self.prekey], axis=1)
      fc = tf.contrib.layers.fully_connected(
          combined_message,
          TEXT_SIZE + KEY_SIZE,
          biases_initializer=tf.constant_initializer(0.0),
          activation_fn=None)

      fc = tf.expand_dims(fc, 2)
      conv = tf.contrib.layers.conv2d(
          fc, 2, 2, 2, 'SAME', activation_fn=tf.nn.sigmoid)
      conv = tf.contrib.layers.conv2d(
          conv, 2, 1, 1, 'SAME', activation_fn=tf.nn.sigmoid)
      conv = tf.contrib.layers.conv2d(
          conv, 1, 1, 1, 'SAME', activation_fn=tf.nn.tanh)
      conv = tf.squeeze(conv, 2)
      return conv
  def model_attack2(self, collection, message, key=None):
    if key is not None:
      combined_message = tf.concat(axis=1, values=[message, key])
    else:
      combined_message = message

    # Ensure that all variables created are in the specified collection.
    with tf.contrib.framework.arg_scope(
        [tf.contrib.layers.fully_connected, tf.contrib.layers.conv2d],
        variables_collections=[collection]):

      fc = tf.contrib.layers.fully_connected(
          combined_message,
          TEXT_SIZE + KEY_SIZE,
          biases_initializer=tf.constant_initializer(0.0),
          activation_fn=None)

      fc = tf.expand_dims(fc, 2)
      conv = tf.contrib.layers.conv2d(
          fc, 2, 2, 2, 'SAME', activation_fn=tf.nn.sigmoid)
      conv = tf.contrib.layers.conv2d(
          conv, 2, 1, 1, 'SAME', activation_fn=tf.nn.sigmoid)
      conv = tf.contrib.layers.conv2d(
          conv, 1, 1, 1, 'SAME', activation_fn=tf.nn.tanh)
      conv = tf.squeeze(conv, 2)

      self.prekey = conv

      combined_message = tf.concat([message, self.prekey], axis=1)
      fc = tf.contrib.layers.fully_connected(
          combined_message,
          TEXT_SIZE + KEY_SIZE,
          biases_initializer=tf.constant_initializer(0.0),
          activation_fn=None)

      fc = tf.expand_dims(fc, 2)
      conv = tf.contrib.layers.conv2d(
          fc, 2, 2, 2, 'SAME', activation_fn=tf.nn.sigmoid)
      conv = tf.contrib.layers.conv2d(
          conv, 2, 1, 1, 'SAME', activation_fn=tf.nn.sigmoid)
      conv = tf.contrib.layers.conv2d(
          conv, 1, 1, 1, 'SAME', activation_fn=tf.nn.tanh)
      conv = tf.squeeze(conv, 2)
      return conv

  def __init__(self, FLAGS, length):
    self.FLAGS = FLAGS
    self.TEXT_SIZE = length
    self.KEY_SIZE = length
    self.temp = tf.Variable(options.temperature, name="temperature", trainable=False)
    pass

  def collect_reset_eve_vars(self, ver='original'):
    if ver == 'original':
      self.reset_eve_vars = self.original_reset_eve_vars
    elif ver == 'real':
      self.reset_eve_vars = self.real_reset_eve_vars
    else:
      print('illegal usage of reset eve vars')
      exit(1)
  def build(self, version, malice='default', mbob='default', meve='default'):
    in_m, in_k = self.get_message_and_key(version)
    self.version = version
    self.encrypted = self.model('alice', in_m, in_k, malice)
    self.decrypted = self.model('bob', self.encrypted, in_k, mbob)
    self.eve_out = self.model('eve', self.encrypted, None, meve)
    #carol_out = self.model('carol', encrypted, None)

    # original reset eve vars
    self.original_reset_eve_vars = tf.group(
        *[w.initializer for w in tf.get_collection('eve')])

    self.collect_reset_eve_vars()

    optimizer = tf.train.AdamOptimizer(learning_rate=FLAGS.learning_rate)

    self.get_eve_loss(in_m)
    self.eve_optimizer = optimizer.minimize(
        self.eve_loss, var_list=tf.get_collection('eve'))
    #self.carol_optimizer = optimizer.minimize(
    #    self.carol_loss, var_list=tf.get_collection('carol'))

    # real reset eve vars including adam and beta for the purpose of indead resetting eve
    var_list = tf.get_collection('eve')
    opteve = [optimizer.get_slot(var, name) for name in optimizer.get_slot_names() for var in var_list]
    self.real_reset_eve_vars = tf.group(
        *([w.initializer for w in var_list] + [w.initializer for w in opteve] + [w.initializer for w in tf.global_variables() if 'beta' in w.name]))

    self.get_bob_loss(in_m)
    self.bob_optimizer = optimizer.minimize(
        self.bob_loss,
        var_list=(tf.get_collection('alice') + tf.get_collection('bob')))

    self.get_bob_metrics(in_m)
  def get_eve_loss(self, in_m):
    # Eve's goal is to decrypt the entire message:
    self.eve_bits_wrong = tf.reduce_sum(
        tf.abs((self.eve_out + 1.0) / 2.0 - (in_m + 1.0) / 2.0), [1])
    #carol_bits_wrong = tf.reduce_sum(
    #    tf.abs((carol_out + 1.0) / 2.0 - (in_k + 1.0) / 2.0), [1])
    self.eve_loss = tf.reduce_sum(self.eve_bits_wrong)
    self.eve_reconstruction_loss = self.eve_loss
    #self.carol_loss = tf.reduce_sum(carol_bits_wrong)
  def get_bob_loss(self, in_m):
    # Alice and Bob want to be accurate...
    self.bob_bits_wrong = tf.reduce_sum(
        tf.abs((self.decrypted + 1.0) / 2.0 - (in_m + 1.0) / 2.0), [1])
    # ... and to not let Eve do better than guessing.
    self.bob_reconstruction_loss = tf.reduce_sum(self.bob_bits_wrong)
    bob_eve_error_deviation = tf.abs(float(TEXT_SIZE) / 2.0 - self.eve_bits_wrong)
    #bob_carol_error_deviation = tf.abs(float(TEXT_SIZE) / 2.0 - carol_bits_wrong)

    # 7-9 bits wrong is OK too, so we squish the error function a bit.
    # Without doing this, we often tend to hang out at 0.25 / 7.5 error,
    # and it seems bad to have continued, high communication error.
    bob_eve_loss = tf.reduce_sum(
        tf.square(bob_eve_error_deviation) / (TEXT_SIZE / 2)**2)
    #bob_carol_loss = tf.reduce_sum(
    #    tf.square(bob_carol_error_deviation) / (TEXT_SIZE / 2)**2)

    # Rescale the losses to [0, 1] per example and combine.
    self.bob_loss = (self.bob_reconstruction_loss / TEXT_SIZE + bob_eve_loss)
  def get_bob_metrics(self, in_m):
    #metrics
    self.bob_decryp_error = tf.reduce_sum(tf.abs(tf.round(self.decrypted) - in_m) / 2)
  def build_att(self, version, malice='default', mbob='default', meve='default'):
    in_m, in_k = self.get_message_and_key(version)
    self.version = version
    self.encrypted = self.model('alice', in_m, in_k, malice)
    self.decrypted = self.model('bob', self.encrypted, in_k, mbob)
    self.eve_out = self.model('eve', self.encrypted, None, meve)
    #carol_out = self.model('carol', encrypted, None)

    # original reset eve vars
    self.original_reset_eve_vars = tf.group(
        *[w.initializer for w in tf.get_collection('eve')])

    self.alice_bob_vars = (
        ([w for w in tf.get_collection('alice')] + [w for w in tf.get_collection('bob')]))

    optimizer = tf.train.AdamOptimizer(learning_rate=FLAGS.learning_rate)

    self.get_eve_loss_att(in_m)
    self.eve_optimizer = optimizer.minimize(
        self.eve_loss, var_list=tf.get_collection('eve'))
    #self.carol_optimizer = optimizer.minimize(
    #    self.carol_loss, var_list=tf.get_collection('carol'))

    # real reset eve vars including adam and beta for the purpose of indead resetting eve
    var_list = tf.get_collection('eve')
    opteve = [optimizer.get_slot(var, name) for name in optimizer.get_slot_names() for var in var_list]
    self.real_reset_eve_vars = tf.group(
        *([w.initializer for w in var_list] + [w.initializer for w in opteve] + [w.initializer for w in tf.global_variables() if 'beta' in w.name]))

    self.collect_reset_eve_vars('real')

    self.get_bob_loss(in_m)

    self.get_bob_metrics(in_m)

  def get_eve_loss_att(self, in_m):
    # Eve's goal is to decrypt the entire message:
    self.eve_bits_wrong = tf.reduce_sum(
        tf.abs((self.eve_out + 1.0) / 2.0 - (in_m + 1.0) / 2.0), [1])
    #carol_bits_wrong = tf.reduce_sum(
    #    tf.abs((carol_out + 1.0) / 2.0 - (in_k + 1.0) / 2.0), [1])
    self.eve_loss = tf.reduce_sum(self.eve_bits_wrong)
    self.eve_reconstruction_loss = self.eve_loss
    #self.carol_loss = tf.reduce_sum(carol_bits_wrong)
    if options.bkey == 1:
      self.eve_loss += tf.reduce_sum(tf.abs((self.prekey + 1.0) / 2.0 - (self.in_k + 1.0) / 2.0))

  def build_view(self, version, malice='default', mbob='default'):
    in_m, in_k = self.get_message_and_key(version)
    self.encrypted = self.model('alice', in_m, in_k, malice)
    self.decrypted = self.model('bob', self.encrypted, in_k, mbob)

    self.alice_bob_vars = (
        ([w for w in tf.get_collection('alice')] + [w for w in tf.get_collection('bob')]))

    self.bob_bits_wrong = tf.reduce_sum(
        tf.abs((self.decrypted + 1.0) / 2.0 - (in_m + 1.0) / 2.0), [1])
    self.bob_reconstruction_loss = tf.reduce_sum(self.bob_bits_wrong)
    self.get_bob_metrics(in_m)

