import tensorflow as tf
from tensorflow.python.framework import ops
import numpy as np

#user define activation function
def mysig(x):
	if x < -3:
		return -1.
	elif x > 3:
		return 1.
	else:
		#return 1 / (1 + np.exp(-x))
		return np.tanh(x)

def myd(x):
	if x < -3:
		return 1e0
	elif x > 3:
		return 1e0
	else:
		#return np.exp(-x) / (1 + np.exp(-x))**2
		return 1. / np.cosh(x)**2

np_mysig = np.vectorize(mysig)
np_myd = np.vectorize(myd)

np_myd_32 = lambda x: np_myd(x).astype(np.float32)
def tf_myd(x, name=None):
	with ops.op_scope([x], name, 'myd') as name:
		y = tf.py_func(np_myd_32, [x], [tf.float32], name=name, stateful=False)
		return y[0]

def py_func(func, inp, Tout, stateful=True, name=None, grad=None):
	rnd_name = 'PFG' + str(np.random.randint(0, 1e8))
	tf.RegisterGradient(rnd_name)(grad)
	g = tf.get_default_graph()
	with g.gradient_override_map({"PyFunc": rnd_name}):
		return tf.py_func(func, inp, Tout, stateful=stateful, name=name)

def spikygrad(op, grad):
	x = op.inputs[0]
	n_gr = tf_myd(x)
	return grad * n_gr

np_spiky_32 = lambda x: np_mysig(x).astype(np.float32)
def tf_spiky(x, name=None):
	with ops.op_scope([x], name, 'spiky') as name:
		y = py_func(np_spiky_32, [x], [tf.float32], name=name, grad=spikygrad)
		return y[0]
#user define activation function end#

