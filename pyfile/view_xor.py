# Copyright 2016 The TensorFlow Authors All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Adversarial training to learn trivial encryption functions,
from the paper "Learning to Protect Communications with
Adversarial Neural Cryptography", Abadi & Andersen, 2016.

https://arxiv.org/abs/1610.06918

This program creates and trains three neural networks,
termed Alice, Bob, and Eve.  Alice takes inputs
in_m (message), in_k (key) and outputs 'ciphertext'.

Bob takes inputs in_k, ciphertext and tries to reconstruct
the message.

Eve is an adversarial network that takes input ciphertext
and also tries to reconstruct the message.

The main function attempts to train these networks and then
evaluates them, all on random plaintext and key values.

"""

# TensorFlow Python 3 compatibility
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import signal
import sys
from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf
import numpy as np
from argparse import ArgumentParser
import copy
from myparser import options


flags = tf.app.flags

flags.DEFINE_float('learning_rate', 0.0008, 'Constant learning rate')
flags.DEFINE_integer('batch_size', 4096, 'Batch size')

FLAGS = flags.FLAGS

# Input and output configuration.
TEXT_SIZE = options.len
KEY_SIZE = options.len

# Training parameters.
ITERS_PER_ACTOR = 1
EVE_MULTIPLIER = 2  # Train Eve 2x for every step of Alice/Bob
# Train until either max loops or Alice/Bob "good enough":
MAX_TRAINING_LOOPS = 850000
    # Exit when Bob loss < BOB_LOSS_THRESH and Eve > EVE_LOSS_THRESH bits
BOB_LOSS_THRESH_PERCENT = 1. / 160
BOB_LOSS_THRESH_PERCENT = 1. / 800
BOB_LOSS_THRESH = BOB_LOSS_THRESH_PERCENT * TEXT_SIZE  # Exit when Bob loss < 0.02 and Eve > 7.7 bits
EVE_LOSS_THRESH_PERCENT = 0.4
EVE_LOSS_THRESH_PERCENT = 0.43
EVE_LOSS_THRESH = EVE_LOSS_THRESH_PERCENT * TEXT_SIZE

# Logging and evaluation.
PRINT_EVERY = 200  # In training, log every 200 steps.
EVE_EXTRA_ROUNDS = 15000  # At end, train eve a bit more.
RETRAIN_EVE_ITERS = 10000  # Retrain eve up to ITERS*LOOPS times.
RETRAIN_EVE_LOOPS = 25  # With an evaluation each loop
NUMBER_OF_EVE_RESETS = 5  # And do this up to 5 times with a fresh eve.
# Use EVAL_BATCHES samples each time we check accuracy.
EVAL_BATCHES = 1

def bit_representation(num, n):
	r = [[] for _ in range(n)]
	for i in range(n):
		r[i] = (num & (1 << i))
		if r[i] == 0:
			r[i] = -1
		else:
			r[i] = 1
	return r

def batch_of_value(batch_size, n, fixed=False, key=None):
  if key != None:
    assert fixed
    assert key.shape == (1, n)
    as_int = key
  else:
    if fixed:
      size = (1, n)
    else:
      size = (batch_size, n)
    as_int = np.random.uniform(-1, 1, size=size)
    as_int = np.where(as_int >= 0, 1, -1)
  if fixed:
    as_int = np.repeat(as_int, batch_size, axis=0)
  return np.array(as_int, dtype=np.float32)


class AdversarialCrypto(object):
  """Primary model implementation class for Adversarial Neural Crypto.

  This class contains the code for the model itself,
  and when created, plumbs the pathways from Alice to Bob and
  Eve, creates the optimizers and loss functions, etc.

  Attributes:
    eve_loss:  Eve's loss function.
    bob_loss:  Bob's loss function.  Different units from eve_loss.
    eve_optimizer:  A tf op that runs Eve's optimizer.
    bob_optimizer:  A tf op that runs Bob's optimizer.
    bob_reconstruction_loss:  Bob's message reconstruction loss,
      which is comparable to eve_loss.
    reset_eve_vars:  Execute this op to completely reset Eve.
  """

  def get_message_and_key(self):
    """Generate random pseudo-boolean key and message values."""

    self.in_m = tf.placeholder(shape=[FLAGS.batch_size, TEXT_SIZE], dtype=tf.float32)
    self.in_k = tf.placeholder(shape=[FLAGS.batch_size, TEXT_SIZE], dtype=tf.float32)
    in_m = self.in_m
    in_k = self.in_k
    return in_m, in_k

  def model(self, collection, message, key=None):
    """The model for Alice, Bob, and Eve.  If key=None, the first FC layer
    takes only the message as inputs.  Otherwise, it uses both the key
    and the message.

    Args:
      collection:  The graph keys collection to add new vars to.
      message:  The input message to process.
      key:  The input key (if any) to use.
    """

    if key is not None:
      combined_message = tf.concat(axis=1, values=[message, key])
    else:
      combined_message = message

    # Ensure that all variables created are in the specified collection.
    with tf.contrib.framework.arg_scope(
        [tf.contrib.layers.fully_connected, tf.contrib.layers.conv2d],
        variables_collections=[collection]):

      fc = tf.contrib.layers.fully_connected(
          combined_message,
          TEXT_SIZE + KEY_SIZE,
          biases_initializer=tf.constant_initializer(0.0),
          activation_fn=None)

      # Perform a sequence of 1D convolutions (by expanding the message out to 2D
      # and then squeezing it back down).
      fc = tf.expand_dims(fc, 2)
      # 2,1 -> 1,2
      conv = tf.contrib.layers.conv2d(
          fc, 2, 2, 2, 'SAME', activation_fn=tf.nn.sigmoid)
      # 1,2 -> 1, 2
      conv = tf.contrib.layers.conv2d(
          conv, 2, 1, 1, 'SAME', activation_fn=tf.nn.sigmoid)
      # 1,2 -> 1, 1
      conv = tf.contrib.layers.conv2d(
          conv, 1, 1, 1, 'SAME', activation_fn=tf.nn.tanh)
      conv = tf.squeeze(conv, 2)
      return conv

  def __init__(self):
    in_m, in_k = self.get_message_and_key()
    self.encrypted = self.model('alice', in_m, None)
    #self.encrypted *= 100
    #self.encrypted = tf.minimum(self.encrypted, tf.constant(1.))
    #self.encrypted = tf.maximum(self.encrypted, tf.constant(-1.))
    self.decrypted = self.model('bob', self.encrypted, None)

    # Alice and Bob want to be accurate...
    self.bob_bits_wrong = tf.reduce_sum(
        tf.abs((self.decrypted + 1.0) / 2.0 - (in_m + 1.0) / 2.0), [1])
    # ... and to not let Eve do better than guessing.
    self.bob_reconstruction_loss = tf.reduce_sum(self.bob_bits_wrong)

    #metrics
    self.bob_decryp_error = tf.reduce_sum(tf.abs(tf.round(self.decrypted) - in_m) / 2)

def train_until_thresh(s, ac):
  batch_size = FLAGS.batch_size
  ac.saver = tf.train.Saver()
  #ac.saver = tf.train.Saver(ac.alice_bob_vars)

  filename = options.loadmodel
  ac.saver.restore(s, filename)

  return True

def view_specify_message(msg):
	assert len(msg) > 20
	for i in range(20):
		msg[i] = msg[0]
	tmp = copy.copy(msg[0])
	tmp[0] = tmp[0] * -1
	msg[1] = tmp
	tmp[1] = tmp[1] * -1
	msg[2] = tmp
	tmp = tmp * -1
	msg[3] = tmp
	return msg

def train_and_evaluate():
  """Run the full training and evaluation loop."""

  ac = AdversarialCrypto()
  init = tf.global_variables_initializer()

  with tf.Session() as s:
    s.run(init)
    print('# Batch size: ', FLAGS.batch_size)

    if train_until_thresh(s, ac):
      pass

    batch_size = FLAGS.batch_size
    if options.bkey == 0:
      bkey = False
    else:
      bkey = True
    if options.bmessage == 0:
      bmessage = False
    else:
      bmessage = True
    message = batch_of_value(batch_size, TEXT_SIZE, fixed=bmessage)
    key = batch_of_value(batch_size, TEXT_SIZE, fixed=bkey)
    message = view_specify_message(message)
    #key = view_specify_message(key)
    fd = {ac.in_m: message, ac.in_k: key}
    alice_out, bob_error = s.run([ac.encrypted, ac.bob_decryp_error], feed_dict=fd)
    cipher_differ = copy.copy(alice_out)
    for i in range(1, 5):
    	cipher_differ[i] = cipher_differ[i] - alice_out[i - 1]
    bob_metric = TEXT_SIZE - bob_error / batch_size
    np.set_printoptions(precision=2, formatter={'float': '{: 0.2f}'.format})
    print('message\n', message[:5])
    print('key\n', key[:5])
    print('cipher\n', alice_out[:5])
    print('cipher diff\n', cipher_differ[:5])
    print('bob decrypt accuracy: {:8.3f}'.format(bob_metric))


def main(unused_argv):
  # Exit more quietly with Ctrl-C.
  signal.signal(signal.SIGINT, signal.SIG_DFL)
  train_and_evaluate()


if __name__ == '__main__':
  tf.app.run()
