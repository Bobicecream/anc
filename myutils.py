import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import seaborn as sns
import sys
import numpy as np
import os
from myconst import *
from time import time
from datetime import datetime

def critical():
  print('error')
  print('error')
  print('error')
  print('error')
  print('error')
  assert(0)
  exit(1)
def batch_of_value(batch_size, n, fixed=False, key=None):
  """
  return a batch of data
  fixed: a batch of the same data
  key: feed one data and return a batch of this data
  """
  if key != None:
    assert fixed
    assert key.shape == (1, n)
    as_int = key
  else:
    if fixed:
      size = (1, n)
    else:
      size = (batch_size, n)
    as_int = np.random.uniform(-1, 1, size=size)
    as_int = np.where(as_int >= 0, 1, -1)
  if fixed:
    as_int = np.repeat(as_int, batch_size, axis=0)
  return np.array(as_int, dtype=np.float32)

def plot_errors(ac, ndir, picture, maxlen):
    ratio_plot_errors(ac.history_bob_errors, ac.history_eve_errors, os.path.join(ndir, picture + '-full.png'), maxlen, 1.)
    ratio_plot_errors(ac.history_bob_errors, ac.history_eve_errors, os.path.join(ndir, picture + '-twoh_per.png'), maxlen, 0.005)
def ratio_plot_errors(bob_errors, eve_errors, picture, maxlen, ratio):
    """
    Plot Lowest Decryption Errors achieved by Bob and Eve per step
    """
    sns.set_style("darkgrid")
    tmp = bob_errors[::int(1 / ratio)]
    ltmp = range(1, len(bob_errors) + 1, int(1 / ratio))
    plt.plot(ltmp, tmp)
    tmp = eve_errors[::int(1 / ratio)]
    ltmp = range(1, len(eve_errors) + 1, int(1 / ratio))
    plt.plot(ltmp, tmp)
    plt.legend(['bob', 'eve'])
    plt.xlabel('steps')
    plt.ylabel('Bits Wrong (max:{:d})'.format(maxlen))
    #plt.show()
    plt.savefig(picture)
    plt.close()

def doeval_promotion(s, ac, n, itercount, bprint=True):
  return doeval(s, ac, n, itercount, bprint, None)
def doeval(s, ac, n, itercount, bprint=True, version=None):
  """Evaluate the current network on n batches of random examples.

  Args:
    s:  The current TensorFlow session
    ac: an instance of the AdversarialCrypto class
    n:  The number of iterations to run.
    itercount: Iteration count label for logging.

  Returns:
    Bob and eve's loss, as a percent of bits incorrect.
  """

  bob_loss_accum = 0
  eve_loss_accum = 0
  carol_loss_accum = 0
  bob_metrics_accum = 0
  for _ in range(n):
    if ac.version == 'feed key':
      key = batch_of_value(FLAGS.batch_size, KEY_SIZE)
      tmpcl = getattr(ac, "carol_reconstruction_loss", tf.constant(0.0))
      bl, el, cl = s.run([ac.bob_reconstruction_loss,
                          ac.eve_reconstruction_loss,
                          tmpcl], feed_dict={ac.in_k:key})
      #bl, el = s.run([ac.bob_reconstruction_loss, ac.eve_loss], feed_dict={ac.in_k:key})
      bm = s.run(ac.bob_decryp_error, feed_dict={ac.in_k: key})
    elif ac.version == 'random key':
      tmpcl = getattr(ac, "carol_reconstruction_loss", tf.constant(0.0))
      bl, el, cl = s.run([ac.bob_reconstruction_loss,
                          ac.eve_reconstruction_loss,
                          tmpcl])
      #bl, el = s.run([ac.bob_reconstruction_loss, ac.eve_loss])
      bm = s.run(ac.bob_decryp_error)
      #print('error')
      #exit(0)
    else:
      print('unknown version')
      exit(1)
    bob_loss_accum += bl
    eve_loss_accum += el
    carol_loss_accum += cl
    bob_metrics_accum += bm
  bob_loss_percent = bob_loss_accum / (n * FLAGS.batch_size)
  eve_loss_percent = eve_loss_accum / (n * FLAGS.batch_size)
  carol_loss_percent = carol_loss_accum / (n * FLAGS.batch_size)
  bob_metrics_percent = bob_metrics_accum / (n * FLAGS.batch_size)
  if bprint:
    print('%6d %7.3f %7.3f %7.3f' % (itercount, bob_loss_percent, eve_loss_percent, carol_loss_percent))
    print('bob decrypt metrics = {:.7f}'.format(TEXT_SIZE-bob_metrics_percent))
    sys.stdout.flush()
  return bob_loss_percent, eve_loss_percent, carol_loss_percent

def save_parameters(s, ac, savemodel, ite):
  filepath = os.path.join(savemodel, 'cry-ite{:d}'.format(ite))
  filepath = ac.saver.save(s, filepath)
  return filepath
def save_parameters_path(s, ac, savemodel, filename, ite):
  filepath = os.path.join(savemodel, filename)
  filepath = ac.saver.save(s, filepath)
  return filepath

def save_history_errors(ac, ndir, pre=''):
  np.save(os.path.join(ndir, pre + 'bob_errors.npy'), ac.history_bob_errors)
  np.save(os.path.join(ndir, pre + 'eve_errors.npy'), ac.history_eve_errors)
def load_history_errors(ac, ndir, pre=''):
  np.load(os.path.join(ndir, pre + 'bob_errors.npy'), ac.history_bob_errors)
  np.load(os.path.join(ndir, pre + 'eve_errors.npy'), ac.history_eve_errors)

def initg():
  initlog()
def initlog():
  global ticks
  ticks = (time(), datetime.now())
def savelog(modelfile):
  global ticks
  logpos = os.path.join(options.savemodel, 'log')
  with open(logpos, 'w') as logfile:
    nowtime = time()
    nowdate = datetime.now()
    logfile.write('It is in {:s}\n'.format(options.savemodel))
    tmp = ticks[1]
    logfile.write('Start time: {0:4d}-{1:02d}-{2:02d}  {3:2d}:{4:02d}:{5:02d}\n'.format(tmp.year, tmp.month, tmp.day, tmp.hour, tmp.minute, tmp.second))
    tmp = datetime.now()
    logfile.write('End time:   {0:4d}-{1:02d}-{2:02d}  {3:2d}:{4:02d}:{5:02d}\n'.format(tmp.year, tmp.month, tmp.day, tmp.hour, tmp.minute, tmp.second)) 
    logfile.write('Runing time: {:5d}secs\n'.format(int(nowtime - ticks[0])))
    tmp = os.path.basename(modelfile)
    tmp = os.path.splitext(tmp)[0]
    logfile.write('Model data is saved as {:>15s}\n'.format(tmp))
    logfile.write('Command line:\n{:s}\n'.format('python3 ' + ' '.join(sys.argv)))
