from argparse import ArgumentParser

def build_parser():
    parser = ArgumentParser()
    parser.add_argument('--load-model', type=str, dest='loadmodel', help='load model', metavar='FILE')
    parser.add_argument('--save-model', type=str, dest='savemodel', help='save model', metavar='FILE')
    parser.add_argument('--model', type=str, default='default', dest='model_type', help='model type', metavar='TYPE')
    parser.add_argument('--emodel', type=str, default='default', dest='eve_model_type', help='eve model type', metavar='TYPE')
    parser.add_argument('--cmodel', type=str, default='default', dest='car_model_type', help='carol model type', metavar='TYPE')
    parser.add_argument('--params-model', type=str, default=None, dest='params_model', help='', metavar='')
    parser.add_argument('--attack', '-att', type=str, default='attack',
                        dest='attack_type', help='attack model type', metavar='TYPE')
    parser.add_argument('-k', action='store_true', dest='bkey', help='fixed key')
    parser.add_argument('-m', action='store_true', dest='bmsg', help='fixed message')
    parser.add_argument('--len', type=int, default=16, dest='len',
                        choices=[16, 64, 112], help='length of key and message', metavar='LENGTH')
    parser.add_argument('--batch-size', type=int, default=4096, dest='batch_size', help='batch size', metavar='BATCHSIZE')
    parser.add_argument('--learning-rate', type=float, default=0.0008,
                        dest='learning_rate', help='learning rate', metavar='LEARNINGRATE')
    parser.add_argument('--temperature', '--temp', type=float, default=1.0,
                        dest='temperature', help='softmax temperature', metavar='TEMPERATURE')
    parser.add_argument('--phase1', '-p1', type=int, default=60000,
                        dest='phase1', help='at least steps in Fixed Key Phase', metavar='STEP')
    parser.add_argument('--phase2', '-p2', type=int, default=30000,
                        dest='phase2', help='at least steps in Random Key Phase', metavar='STEP')
    parser.add_argument('--eve-mul', type=int, default=2,
                        dest='eve_mul', help='how multiple iterations of training eve than bob', metavar='EVEMUL')
    parser.add_argument('--re-eve-ite', type=int, default=10000,
                        dest='re_eve_ite', help='the number of iterations of retraining eve', metavar='REEVEITE')
    parser.add_argument('--eval-num', type=int, default=1, dest='eval_bat',
                        help='the multiple of the batch size for evaluation', metavar='EVALBAT')
    parser.add_argument('--bob-thr', type=float, default=None, dest='bob_thr', help='bob loss threshold percent')
    parser.add_argument('--eve-thr', type=float, default=None, dest='eve_thr', help='eve loss threshold percent')
    parser.add_argument('--carol-thr', type=float, default=None, dest='carol_thr', help='carol loss threshold percent')
    parser.add_argument('--test-correct', '-tc', action='store_true', dest='tc',
                        help='test the files syntax correctness')
    return parser

parser = build_parser()
options = parser.parse_args()
