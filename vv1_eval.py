# TensorFlow Python 3 compatibility
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import signal
import os
from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf
from AdversarialCrypto import AdversarialCrypto
from myconst import *
from myutils import *

#check required of parameters
assert options.savemodel != None
assert not options.bkey
assert not options.bmsg
assert os.path.exists(options.savemodel)

def train_until_thresh(s, ac):
  batch_size = FLAGS.batch_size
  ac.saver = tf.train.Saver(max_to_keep=5)
  ac.history_bob_errors, ac.history_eve_errors = [], []

  initg()

  for j in xrange(MAX_TRAINING_LOOPS):
    for _ in xrange(ITERS_PER_ACTOR):
      key = batch_of_value(batch_size, KEY_SIZE)
      s.run(ac.bob_optimizer, feed_dict={ac.in_k: key})
    for _ in xrange(ITERS_PER_ACTOR * EVE_MULTIPLIER):
      key = batch_of_value(batch_size, KEY_SIZE, fixed=True)
      s.run(ac.eve_optimizer, feed_dict={ac.in_k: key})
    bob_avg_loss, eve_avg_loss, _ = doeval_promotion(s, ac, EVAL_BATCHES, j, False)
    ac.history_eve_errors.append(eve_avg_loss)
    ac.history_bob_errors.append(bob_avg_loss)
    if j % PRINT_EVERY == 0:
      bob_avg_loss, eve_avg_loss, _ = doeval_promotion(s, ac, EVAL_BATCHES, j)
      if j < FKP_ITE:
        continue
      if (bob_avg_loss < BOB_LOSS_THRESH and eve_avg_loss > EVE_LOSS_THRESH):
        filepath = save_parameters_path(s, ac, options.savemodel, 'v1p1-ite{:d}'.format(j), j)
        break
  save_history_errors(ac, options.savemodel, 'fkp-')
  ac.history_bob_errors, ac.history_eve_errors = [], []

  s.run(ac.reset_eve_vars)
  for j in xrange(MAX_TRAINING_LOOPS):
    for _ in xrange(ITERS_PER_ACTOR):
      key = batch_of_value(batch_size, KEY_SIZE)
      s.run(ac.bob_optimizer, feed_dict={ac.in_k: key})
    for _ in xrange(ITERS_PER_ACTOR * EVE_MULTIPLIER):
      key = batch_of_value(batch_size, KEY_SIZE, fixed=False)
      s.run(ac.eve_optimizer, feed_dict={ac.in_k: key})
    bob_avg_loss, eve_avg_loss, _ = doeval_promotion(s, ac, EVAL_BATCHES, j, False)
    ac.history_eve_errors.append(eve_avg_loss)
    ac.history_bob_errors.append(bob_avg_loss)
    if j % (PRINT_EVERY * 40) == 0:
      filepath = save_parameters(s, ac, options.savemodel, j)
    if j % PRINT_EVERY == 0:
      doeval_promotion(s, ac, EVAL_BATCHES, j)
      if j < RKP_ITE:
        continue
      if (bob_avg_loss < BOB_LOSS_THRESH and eve_avg_loss > EVE_LOSS_THRESH):
        filepath = save_parameters(s, ac, options.savemodel, j)
        print('Target losses achieved.')
        print('checkpoint is save to {}'.format(filepath))
        savelog(filepath)
        return True

  return False


def train_and_evaluate():
  """Run the full training and evaluation loop."""

  ac = AdversarialCrypto(FLAGS, 0)
  ac.build('feed key', MODEL_TYPE, MODEL_TYPE, MODEL_TYPE)
  init = tf.global_variables_initializer()

  with tf.Session() as s:
    s.run(init)
    print('# Batch size: ', FLAGS.batch_size)
    print('# Iter Bob_Recon_Error Eve_Recon_Error')

    if train_until_thresh(s, ac):
      pass

    plot_errors(ac, options.savemodel, 'nothing', TEXT_SIZE)
    save_history_errors(ac, options.savemodel, 'rkp-')

    batch_size = FLAGS.batch_size
    key = batch_of_value(batch_size, KEY_SIZE, fixed=True)
    for _ in xrange(EVE_EXTRA_ROUNDS):
      s.run(ac.eve_optimizer, feed_dict={ac.in_k: key})
    print('Loss after eve extra training:')
    doeval_promotion(s, ac, EVAL_BATCHES * 2, 0)

    for _ in xrange(NUMBER_OF_EVE_RESETS):
      print('Resetting Eve {:d}'.format(NUMBER_OF_EVE_RESETS))
      ac.collect_reset_eve_vars('real')
      s.run(ac.reset_eve_vars)
      eve_counter = 0
      eve_reconstruct_error = []
      key = batch_of_value(batch_size, KEY_SIZE, fixed=True)
      for _ in xrange(RETRAIN_EVE_LOOPS):
        for _ in xrange(RETRAIN_EVE_ITERS):
          eve_counter += 1
          s.run(ac.eve_optimizer, feed_dict={ac.in_k: key})
        _, tmp, _ = doeval_promotion(s, ac, EVAL_BATCHES, eve_counter)
        eve_reconstruct_error += [tmp]
      print('The lowest reconstruction error of Eve is {:.3f}.'.format(min(eve_reconstruct_error)))


def main(unused_argv):
  # Exit more quietly with Ctrl-C.
  signal.signal(signal.SIGINT, signal.SIG_DFL)
  train_and_evaluate()


if __name__ == '__main__':
  tf.app.run()

