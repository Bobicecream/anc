# Copyright 2016 The TensorFlow Authors All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Adversarial training to learn trivial encryption functions,
from the paper "Learning to Protect Communications with
Adversarial Neural Cryptography", Abadi & Andersen, 2016.

https://arxiv.org/abs/1610.06918

This program creates and trains three neural networks,
termed Alice, Bob, and Eve.  Alice takes inputs
in_m (message), in_k (key) and outputs 'ciphertext'.

Bob takes inputs in_k, ciphertext and tries to reconstruct
the message.

Eve is an adversarial network that takes input ciphertext
and also tries to reconstruct the message.

The main function attempts to train these networks and then
evaluates them, all on random plaintext and key values.

"""

"""
We use this file for the evaluation.
The evaluation is used for comparing security of encryptors 
constructed by different versions or models.
"""

# TensorFlow Python 3 compatibility
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import signal
import sys
import os
from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf
import numpy as np
from AdversarialCrypto import AdversarialCrypto
from myutils import *
from myconst import *

# check parameters
assert options.loadmodel != None
assert not options.bmsg
assert options.attack_type != None
assert os.path.exists(options.loadmodel + ".meta")

def restore_checkpoint(s, ac):
  ac.saver = tf.train.Saver(ac.alice_bob_vars)

  filename = options.loadmodel
  ac.saver.restore(s, filename)

  return True


def train_and_evaluate():
  """Run the full training and evaluation loop."""

  version = 'feed key'
  ac = AdversarialCrypto(FLAGS, 0)
  ac.build_att(version, MODEL_TYPE, MODEL_TYPE, ATTACK_TYPE)
  init = tf.global_variables_initializer()

  with tf.Session() as s:
    s.run(init)
    print('# Batch size: ', FLAGS.batch_size)

    restore_checkpoint(s, ac)

    batch_size = FLAGS.batch_size
    for _ in xrange(NUMBER_OF_EVE_RESETS):
      print('Resetting Eve')
      s.run(ac.reset_eve_vars)
      eve_counter = 0
      eve_reconstruct_error = []
      for _ in xrange(RETRAIN_EVE_LOOPS):
        for _ in xrange(RETRAIN_EVE_ITERS):
          eve_counter += 1
          key = batch_of_value(batch_size, TEXT_SIZE, fixed=False)
          s.run(ac.eve_optimizer, feed_dict={ac.in_k: key})
        _, tmp, _ = doeval(s, ac, EVAL_BATCHES, eve_counter, version=version)
        eve_reconstruct_error += [tmp]
      print('The lowest reconstruction error of Eve is {:.3f}.'.format(min(eve_reconstruct_error)))


def main(unused_argv):
  # Exit more quietly with Ctrl-C.
  signal.signal(signal.SIGINT, signal.SIG_DFL)
  train_and_evaluate()


if __name__ == '__main__':
  tf.app.run()
