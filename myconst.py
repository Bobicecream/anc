import tensorflow as tf
from myparser import options

flags = tf.app.flags

flags.DEFINE_float('learning_rate', options.learning_rate, 'Constant learning rate')
flags.DEFINE_integer('batch_size', options.batch_size, 'Batch size')

FLAGS = flags.FLAGS

# Input and output configuration.
TEXT_SIZE = options.len
KEY_SIZE = options.len

# Training parameters.
ITERS_PER_ACTOR = 1
EVE_MULTIPLIER = options.eve_mul  # Train Eve 2x for every step of Alice/Bob
# Train until either max loops or Alice/Bob "good enough":
MAX_TRAINING_LOOPS = 850000
    # Exit when Bob loss < BOB_LOSS_THRESH and Eve > EVE_LOSS_THRESH bits
if options.bob_thr == None:
	options.bob_thr = 1. / 800
BOB_LOSS_THRESH_PERCENT = options.bob_thr
BOB_LOSS_THRESH = BOB_LOSS_THRESH_PERCENT * TEXT_SIZE  # Exit when Bob loss < 0.02 and Eve > 7.7 bits
if options.eve_thr == None:
	options.eve_thr = 0.48
EVE_LOSS_THRESH_PERCENT = options.eve_thr
EVE_LOSS_THRESH = EVE_LOSS_THRESH_PERCENT * TEXT_SIZE

# Logging and evaluation.
PRINT_EVERY = 200  # In training, log every 200 steps.
EVE_EXTRA_ROUNDS = 15000  # At end, train eve a bit more.
RETRAIN_EVE_ITERS = options.re_eve_ite  # Retrain eve up to ITERS*LOOPS times.
RETRAIN_EVE_LOOPS = 25  # With an evaluation each loop
NUMBER_OF_EVE_RESETS = 5  # And do this up to 5 times with a fresh eve.
# Use EVAL_BATCHES samples each time we check accuracy.
EVAL_BATCHES = options.eval_bat

# at least 60000 iterations in Fixed Key Phase
FKP_ITE = options.phase1
# at least 30000 iterations in Random Key Phase
RKP_ITE = options.phase2

# origin_update origin2016 promotion
MODEL_TYPE = options.model_type
EVE_TYPE = options.eve_model_type
CAR_TYPE = options.car_model_type
ATTACK_TYPE = options.attack_type
if type(options.params_model) == str:
  MODEL_PARAMS = options.params_model.split('-')
STAGE = 'rating'

if options.tc:
  BOB_LOSS_THRESH = 0.2 * TEXT_SIZE
  EVE_LOSS_THRESH = 0.3 * TEXT_SIZE
  EVE_EXTRA_ROUNDS = 10
  RETRAIN_EVE_ITERS = 100
  FKP_ITE = 200
  RKP_ITE = 200
  RETRAIN_EVE_LOOPS = 2
  NUMBER_OF_EVE_RESETS = 1
