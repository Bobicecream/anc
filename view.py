# Copyright 2016 The TensorFlow Authors All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Adversarial training to learn trivial encryption functions,
from the paper "Learning to Protect Communications with
Adversarial Neural Cryptography", Abadi & Andersen, 2016.

https://arxiv.org/abs/1610.06918

This program creates and trains three neural networks,
termed Alice, Bob, and Eve.  Alice takes inputs
in_m (message), in_k (key) and outputs 'ciphertext'.

Bob takes inputs in_k, ciphertext and tries to reconstruct
the message.

Eve is an adversarial network that takes input ciphertext
and also tries to reconstruct the message.

The main function attempts to train these networks and then
evaluates them, all on random plaintext and key values.

"""

# TensorFlow Python 3 compatibility
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import signal
import sys
from six.moves import xrange  # pylint: disable=redefined-builtin
import tensorflow as tf
import numpy as np
from copy import copy
from myparser import options
from AdversarialCrypto import AdversarialCrypto
from myconst import *
from myutils import *

# check parameters
assert options.loadmodel != None

def bit_representation(num, n):
	r = [[] for _ in range(n)]
	for i in range(n):
		r[i] = (num & (1 << i))
		if r[i] == 0:
			r[i] = -1
		else:
			r[i] = 1
	return r

def restore_checkpoint(s, ac):
  ac.saver = tf.train.Saver()

  filename = options.loadmodel
  ac.saver.restore(s, filename)

  return True

def view_specify_message(msg):
	assert len(msg) > 20
	for i in range(20):
		msg[i] = msg[0]
	tmp = copy(msg[0])
	tmp[0] = tmp[0] * -1
	msg[1] = tmp
	tmp[1] = tmp[1] * -1
	msg[2] = tmp
	tmp = tmp * -1
	msg[3] = tmp
	return msg

def train_and_evaluate():
  """Run the full training and evaluation loop."""

  ac = AdversarialCrypto(FLAGS, 0)
  ac.build_view('feed msg and key', MODEL_TYPE, MODEL_TYPE)
  init = tf.global_variables_initializer()

  with tf.Session() as s:
    s.run(init)
    print('# Batch size: ', FLAGS.batch_size)

    restore_checkpoint(s, ac)

    batch_size = FLAGS.batch_size
    bkey = options.bkey
    bmessage = options.bmsg

    message = batch_of_value(batch_size, TEXT_SIZE, fixed=bmessage)
    key = batch_of_value(batch_size, TEXT_SIZE, fixed=bkey)
    if bkey:
      message = view_specify_message(message)
    #key = view_specify_message(key)
    fd = {ac.in_m: message, ac.in_k: key}
    alice_out, bob_error = s.run([ac.encrypted, ac.bob_decryp_error], feed_dict=fd)

    cipher_differ = copy(alice_out)
    for i in range(1, 5):
    	cipher_differ[i] = cipher_differ[i] - alice_out[i - 1]
    bob_metric = TEXT_SIZE - bob_error / batch_size
    np.set_printoptions(precision=2, formatter={'float': '{: 0.2f}'.format})
    if bmessage:
      print('message\n', message[:1])
    else:
      print('message\n', message[:5])
    if bkey:
      print('key\n', key[:1])
    else:
      print('key\n', key[:5])
    print('cipher\n', alice_out[:5])
    print('cipher diff\n', cipher_differ[:5])
    print('bob decrypt accuracy: {:8.3f}'.format(bob_metric))
    if bkey and bmessage:
      print('-m -k\tthis function not implement')


def main(unused_argv):
  # Exit more quietly with Ctrl-C.
  signal.signal(signal.SIGINT, signal.SIG_DFL)
  train_and_evaluate()


if __name__ == '__main__':
  tf.app.run()
